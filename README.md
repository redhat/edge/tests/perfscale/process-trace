# process-trace

This is a script intended to post-process output from `ltrace` into function time summary.
This is similar to what is provided by `ltrace -c`, but that command requires the traced
command to end before generating its summary data, and this script is instead intended
to process an existing ltrace output file (even one that is still open for streaming).

This assumes `ltrace` was run with this set of flags:
```
ltrace -CSTftt -n 2 -o <ltrace output file> <command to trace>
```

Simply run the process script against the output file:
```
process-ltrace.sh <ltrace output file>
```

The script will find all library and system calls with the same name, add up the total
time spend in each call, and return the total time, name of the call, and number of 
calls made.
```
# ./process-ltrace.sh /var/tmp/NetworkManager-ltrace-2024-01-24-00-00-06.out
Processing...
Processing call 275@SYS...
Processing call 278@SYS...
Processing call 293@SYS...
Processing call 4096)...
Processing call 434@SYS...
Processing call 436@SYS...
...
0.594923   g_dbus_proxy_new_for_bus_sync (1 call)
0.375725   g_object_new (143 calls)
0.229127   g_task_return_pointer (2 calls)
0.049089   g_module_open (1 call)
0.045412   restart_syscall@SYS (1 call)
0.044035   g_initable_new (1 call)
0.018223   g_object_notify_by_pspec (394 calls)
0.017341   curl_global_init (1 call)
0.015745   getnameinfo (1 call)
...
```