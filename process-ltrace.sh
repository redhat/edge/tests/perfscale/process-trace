#! /bin/bash

printf "Processing...\n" >&2

# Get all of the unique calls
calls=$(grep -v -e '+++' -e '\-\-\-' $1 | awk '{if($3=="<...") {print $4} else {print $3}}' | awk -F\( '{print $1}' | sort -u)

# Setup a named pipe for the query output
query_pipe="/tmp/$(tr -dc A-Za-z0-9 </dev/urandom | head -c 8)"
mkfifo $query_pipe

spin='-\|/'

for call in $calls; do
  count=0
  sum=0
  printf "Processing call $call...\n" >&2
  # Find each instance of a call that has a duration value, and add all of the durations
  for time in $(grep -e "${call}(" -e "${call}\ " $1 | awk -F= '{print $2}' | grep -Eo '[0-9]+(\.[0-9]+)'); do
    count=$(( count + 1 ))
    i=$(( count %4 ))
    # Print the spinner indicator
    printf "\r${spin:$i:1}" >&2
    #printf "Adding $time to previous value $sum" >&2
    sum=$(echo "$sum + $time" | bc)
  done
  # Get the pluralization right for the output text
  if [[ $count == 1 ]]; then
    noun="call"
  else
    noun="calls"
  fi
  printf "\r" >&2
  # Send each call summary to the named pipe
  printf "$time   $call ($count $noun)\n" > $query_pipe &
done 

printf "Sorting...\n" >&2

# Print headers
(printf "\nTot Time   Lib/Sys Call";
 printf "\n========   ============\n";
# Sort the values from the query named pipe
 sort -rn < $query_pipe) & 
sort_pid=$!

# Print the spinner indicator while sorting
while ps -p $sort_pid &>/dev/null; do
  i=$(( (i + 1) %4 ))
  printf "\r${spin:$i:1}\r" >&2
  sleep 0.1
done

